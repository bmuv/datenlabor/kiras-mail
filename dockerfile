FROM python:3.11

COPY pyproject.toml .
COPY requirements.txt .

# if you are using a pypi mirror that requires authentification, you can set user credential in your .netrc file
# if you are using the normal pypi index, you must generate an empty .netrc file
COPY .netrc root/.netrc 

ARG PIP_TRUSTED_HOST=pypi.org
ARG PIP_INDEX=https://pypi.org/
ARG PIP_INDEX_URL=https://pypi.org/simple/

ENV PIP_TRUSTED_HOST=${PIP_TRUSTED_HOST} 
ENV PIP_INDEX=${PIP_INDEX}
ENV PIP_INDEX_URL=${PIP_INDEX_URL}

RUN pip install -r requirements.txt
RUN pip freeze > lock.txt

COPY kiras_mail/ ./kiras_mail/


CMD ["python", "-m", "kiras_mail.app"]

