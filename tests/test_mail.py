from kiras_mail.mail import create_mail_body
from kiras_mail.models import (
    ReplyContent,
    ReplySection,
    Agency,
    InvolvementScore,
    Explanation,
)

from exchangelib import HTMLBody
from jinja2 import Environment, FileSystemLoader


def test_create_mail_body():
    text = "<b>Das</b> ist der Bibabutzemann."

    mock_response = ReplyContent(
        email_text=text,
        sections=[
            ReplySection(
                agency=Agency(name="Fabelministerium", abbreviation="FB"),
                results=[
                    InvolvementScore(
                        unit_name="T I 2",
                        score=0.2,
                        explanations=[
                            Explanation(
                                sentence="Das ist ein Satz.",
                                task="Das ist die passende task.",
                                score=0.15,
                            ),
                            Explanation(
                                sentence="Dies ist ein weniger relevanter Satz, der nicht in der Antwort auftauchen sollte.",
                                task="Das ist die passende task.",
                                score=0.15,
                            ),
                        ],
                    ),
                    InvolvementScore(
                        unit_name="Z II 1",
                        score=0.1,
                        explanations=[
                            Explanation(
                                sentence="Das ist ein anderer Satz.",
                                task="Das ist die passende task des Referats Z II 1.",
                                score=0.05,
                            )
                        ],
                    ),
                ],
            ),
            ReplySection(
                agency=Agency(name="Umweltbundesamt", abbreviation="UBA"),
                results=[
                    InvolvementScore(
                        unit_name="T I 2",
                        score=0.2,
                        explanations=[
                            Explanation(
                                sentence="Das ist ein Satz.",
                                task="Das ist die passende task.",
                                score=0.15,
                            ),
                            Explanation(
                                sentence="Dies ist ein weniger relevanter Satz, der nicht in der Antwort auftauchen sollte.",
                                task="Das ist die passende task.",
                                score=0.15,
                            ),
                        ],
                    ),
                    InvolvementScore(
                        unit_name="Z II 1",
                        score=0.1,
                        explanations=[
                            Explanation(
                                sentence="Das ist ein anderer Satz.",
                                task="Das ist die passende task des Referats Z II 1.",
                                score=0.05,
                            )
                        ],
                    ),
                ],
            ),
        ],
    )

    response = create_mail_body(
        original_html_mail=text,
        reply_content=mock_response,
        template="extended.html",
    )

    assert text in response
    assert not "weniger relevante" in response
