import pytest
import requests
import requests_mock

from kiras_mail.kiras_api import KirasAPI


@pytest.fixture
def mock_response_body():
    return {
        "text": "test text",
        "scores": [
            {
                "unit_name": "Referat 1",
                "score": 0.9,
                "explanations": [
                    {
                        "task": "Wegen dieser task ist das Referat betroffen.",
                        "sentence": "Wegen diesem Satz ist das Referat betroffen.",
                        "score": 0.9,
                    }
                ],
            },
            {
                "unit_name": "Referat 2",
                "score": 0.8,
                "explanations": [
                    {
                        "task": "Eine andere task eines anderen Referats.",
                        "sentence": "Hier ergibt sich gerade Betroffenheit.",
                        "score": 0.7,
                    }
                ],
            },
            {
                "unit_name": "Referat 3",
                "score": 0.7,
                "explanations": [
                    {
                        "task": "Untertask von Referat 3.",
                        "sentence": "Wegen diesem Satz ist das Referat betroffen.",
                        "score": 0.7,
                    }
                ],
            },
        ],
    }


@pytest.fixture
def TestKirasAPI(requests_mock):
    mock_agencies = {
        "agencies": [
            {
                "name": "Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz",
                "abbreviation": "BMUV",
            },
            {"name": "Umweltbundesamt", "abbreviation": "UBA"},
        ],
        "default": "BMUV",
    }

    requests_mock.get(
        "http://kiras-api/agencies",
        json=mock_agencies,
    )

    return KirasAPI(endpoint="http://kiras-api")


def test_get_involvement_scores_ok(TestKirasAPI, mock_response_body):
    with requests_mock.Mocker() as m:
        m.get(
            "http://kiras-api/involvement?text=test text&top_k=3",
            json=mock_response_body,
        )

        response = TestKirasAPI.get_involvements(text="test text", top_k=3)
        assert response.text == mock_response_body["text"]
        for i, score in enumerate(response.scores):
            assert score.unit_name == mock_response_body["scores"][i]["unit_name"]
            assert score.score == mock_response_body["scores"][i]["score"]

            for j, explanation in enumerate(score.explanations):
                assert explanation == mock_response_body["scores"][i]["explanations"][j]


def test_get_involvement_scores_invalid_response(TestKirasAPI):
    mock_response_body = {
        "text": "test text",
        "scores": [
            {"unit_name": "Referat 1", "score": 0.9},
            {"unit_name": "Referat 2", "score": 0.8},
            {"unit_name": "Referat 3", "score": "nil"},
        ],
    }
    with requests_mock.Mocker() as m:
        m.get(
            "http://kiras-api/involvement?text=test text&top_k=3",
            json=mock_response_body,
        )
        with pytest.raises(Exception):
            response = TestKirasAPI.get_involvements(text="test text", top_k=3)


def test_get_involvement_scores_404(TestKirasAPI):
    with requests_mock.Mocker() as m:
        m.get("http://kiras-api/involvement?text=test text&top_k=3", status_code=404)
        with pytest.raises(requests.exceptions.HTTPError):
            response = TestKirasAPI.get_involvements(text="test text", top_k=3)


def test_get_agencies(TestKirasAPI):
    response = TestKirasAPI.get_agencies()

    assert (
        response.agencies[0].name
        == "Bundesministerium für Umwelt, Naturschutz, nukleare Sicherheit und Verbraucherschutz"
    )
    assert response.agencies[0].abbreviation == "BMUV"
    assert response.agencies[1].name == "Umweltbundesamt"
    assert response.agencies[1].abbreviation == "UBA"


def test_collect_involvements(TestKirasAPI, mock_response_body):
    with requests_mock.Mocker() as m:
        m.get(
            "http://kiras-api/involvement?text=blabla&top_k=10&agency=BMUV",
            json=mock_response_body,
        )
        response = TestKirasAPI.collect_involvements(text="blabla", mail_subject="BMUV")
        print(response)
        m.get(
            "http://kiras-api/involvement?text=blabla&top_k=10",
            json=mock_response_body,
        )
        response = TestKirasAPI.collect_involvements(text="blabla", mail_subject="Bla")
        print(response)
