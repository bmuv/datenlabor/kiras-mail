# KIRAS-mail

KIRAS-mail receives emails from an Outlook Exchange Server, and obtains involvement scores *from the KIRAS API* for all organizational units of a governmental agency. It then formulates a response email with organizational units ranked by their involvement scores. To explain how KIRAS arrived at the scores, the response contains for each organizational the sentence and the text and the task of the unit that have the largest semantic overlap.

KIRAS-mail can deliver involvement scores for all agencies that are provided by the underlying KIRAS API. The user chooses the agency by including the abbreviation of the agency in the subject line (e.g. "BMUV" in the subject returns organizational units of the "Federal Ministry for the Environment").

## Email Rules
KIRAS-mail uses a set of explicit rules to handle incoming emails:
- new emails are read from the folder `settings.mailbox_folder` of the email address `settings.mail_address`. **NOTE: The developer has to define outlook rules such that emails arrive in this folder. This can be used to define a whitelist of users.**
- if an email sender does not contain `settings.mail_suffix`, it is moved to the `settings.blocked_folder`.
- if the email sender is `settings.mail_address`, it is moved to `settings.blocked_folder`.
- if the subject line of the email contains the `settings.subject_prefix`, KIRAS-mail assumes that it is a feedback response by the user and moves it to `settings.feedback_folder`.
- for all other emails, KIRAS-email generates and sends a response email and then adds `settings.environment` to the email categories of the incoming mail.

## Environments
KIRAS-mail uses `environments`, such that different email addresses can be used for the development and deployment of the project. This allows to use a single `development email address` for all feature branches of the project and a single `production email address` for the master branch.

The environment name is defined as a variable in the gitlab CI pipeline, together with the corresponding mail address and mailbox folder. These settings override the corresponding ones in `settings.py`.


## Local Development
In order to run KIRAS-mail locally, first copy the file `.env-example` to `.env` and insert all credentials and settings as needed.
```
cp .env-example .env
```
Create a suitable python environment (KIRAS-mail requires python>3.10).
Then you can install KIRAS-mail with
```
pip install -r requirements.txt
```
To be able to develop the code and run linting and testing, install with the optional `dev` dependencies:
```
pip install -r requirements-dev.txt
```
Run the code:
```
python -m kiras_mail.app
```
Run tests:
```
pytest tests/
```
## Logging

For logging/printing purposes, you can use: 
```
log.info("Some information")
log.debug("some debugging output")
```
You can set the logging level in the .env file. Possible values are "DEBUG", "INFO", "WARNINGS", "ERRORS". 

## Extract feedback from feedback mails

Users can answer to emails from kiras-mail and provide feedback by ticking checkboxes in the response mail. This feedback can be systematically gathered by running the following script:

```
python scripts/get_feedback.py
```
This writes the feedback to a file `feedback_dataset.csv` in the project root.

## How to adjust KIRAS-mail to your needs
- All parameters of KIRAS-mail are defined in `kiras_mail/settings.py`. You can change the default values of the parameters there, but note that you should not include secrets (login names, passwords, email addresses etc.) there. The best way to define secrets is by copying the file `.env-example`:
```
cp .env-example .env
```
and then changing the parameters in the file `.env`. Values in this file will override those in `kiras_mail/settings.py`. Alternatively, you can define the environment variables directly in your CI pipeline.

## Funding notice
![funding notice](assets/DE_Finanziert_von_der_Europäischen_Union_RG_POS.png)

