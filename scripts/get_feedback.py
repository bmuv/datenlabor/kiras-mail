from datetime import timedelta

import pandas as pd
import urllib3
from exchangelib import (
    DELEGATE,
    NTLM,
    Account,
    Configuration,
    Credentials,
    EWSDateTime,
    EWSTimeZone,
)
from exchangelib.protocol import BaseProtocol, NoVerifyHTTPAdapter

from kiras_mail.settings import settings

urllib3.disable_warnings()

BaseProtocol.HTTP_ADAPTER_CLS = NoVerifyHTTPAdapter

tz = EWSTimeZone("Europe/Berlin")

config = Configuration(
    server="strmail.office.dir",
    credentials=Credentials(
        username=settings.user_name,
        password=settings.password,
    ),
    auth_type=NTLM,
)
print(config)
account = Account(
    primary_smtp_address=settings.mail_address, config=config, access_type=DELEGATE
)
print(account)

feedback_folder = account.inbox / settings.feedback_folder
print(f"checking for mails in folder: {settings.feedback_folder}")


if __name__ == "__main__":
    all = pd.DataFrame()
    now = EWSDateTime.now(tz)
    one_week_ago = now - timedelta(weeks=1)

    new_mails = feedback_folder.filter(datetime_received__gt=one_week_ago).order_by(
        "-datetime_received"
    )

    for mail in new_mails:
        print(f"Processing mail: {mail.sender} {mail.subject}")
        try:
            df = pd.read_html(mail.body, attrs={"id": "scores"})[0]
            all = pd.concat([all, df], ignore_index=True)
        except Exception:
            print(f"Could not parse: {mail.sender} {mail.subject}. Skipping this mail.")
    all.columns = ["Referat", "Aufgabe", "Beispielpassage", "Feedback"]
    all["Feedback"] = all["Feedback"].map({"☒": True, "☐": False})
    all.to_csv("feedback_dataset.csv", index=False)
