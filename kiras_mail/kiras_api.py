"""module for kiras api."""
from typing import List

import requests
from requests.auth import HTTPBasicAuth

from kiras_mail.models import (
    Agency,
    AgencyResponse,
    InvolvementResponse,
    ReplyContent,
    ReplySection,
)
from kiras_mail.settings import settings


class KirasAPI:
    """Client for KIRAS API."""

    def __init__(self, endpoint: str):
        """Set up with endpoint url."""
        self.kiras_api_url = endpoint
        self.available_agencies: List[Agency] = self.get_agencies()
        self.default_agency: Agency = [
            b
            for b in self.available_agencies.agencies
            if b.abbreviation == self.available_agencies.default
        ][0]

    def collect_involvements(self, text, mail_subject: str, top_k=10) -> ReplyContent:
        """Collect involvement scores for agencies mentioned in mail subject."""
        requested_agencies: List[Agency] = self._extract_requested_agencies(
            mail_subject=mail_subject
        )
        reply_sections = []

        for agency in requested_agencies:
            response = self.get_involvements(text, agency.abbreviation, top_k)
            reply_section = ReplySection(agency=agency, results=response.scores)
            reply_sections.append(reply_section)

        return ReplyContent(email_text=text, sections=reply_sections)

    def _extract_requested_agencies(self, mail_subject: str) -> List[Agency]:
        """Extract available agencies from mail subject or fall back to default."""
        requested_agencies = [
            agency
            for agency in self.available_agencies.agencies
            if agency.abbreviation.lower() in mail_subject.lower()
        ]
        if not requested_agencies:
            return [self.default_agency]
        return requested_agencies

    def _call_api(self, path: str, url_params: dict = {}):
        """Call the KIRAS api and return response object or raise error."""
        endpoint = self.kiras_api_url + path
        try:
            response = requests.get(
                endpoint,
                url_params,
                auth=HTTPBasicAuth(
                    settings.kiras_api_login, settings.kiras_api_password
                ),
                verify=False,
            )
            if response.status_code != 200:
                raise requests.exceptions.HTTPError
            return response

        except requests.exceptions.HTTPError as e:
            print(f"HTTP error occurred: {e}")
            raise
        except (ValueError, TypeError) as e:
            print(f"Invalid response received: {e}")
            raise RuntimeError("Failed to parse response from KIRAS-api.")
        except requests.exceptions.RequestException as e:
            print(f"An error occurred while making the request: {e}")
            raise ConnectionError("Failed to connect to KIRAS-api.")

    def get_agencies(self) -> AgencyResponse:
        """Get available governmental agencies from KIRAS api."""
        response = self._call_api(path="/agencies")
        return AgencyResponse.parse_raw(response.text)

    def get_involvements(
        self, text: str, agency_abbrev: str = None, top_k=10
    ) -> InvolvementResponse:
        """Request the KIRAS-api and return scores per unit."""
        url_params = {"text": text, "top_k": top_k}

        if (
            agency_abbrev
            not in [a.abbreviation for a in self.available_agencies.agencies]
            or agency_abbrev is None
        ):
            url_params["agency"] = self.default_agency.abbreviation
        else:
            url_params["agency"] = agency_abbrev

        response = self._call_api(path="/involvement", url_params=url_params)
        return InvolvementResponse.parse_raw(response.text)
