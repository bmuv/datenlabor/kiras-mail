"""Deals with Mails."""

from exchangelib import HTMLBody
from jinja2 import Environment, FileSystemLoader

from kiras_mail.models import ReplyContent


def create_mail_body(
    original_html_mail: str,
    reply_content: ReplyContent = None,
    template="extended.html",
) -> HTMLBody:
    """Create html email body from response of kiras-api."""
    environment = Environment(loader=FileSystemLoader("kiras_mail/templates/"))
    mail_template = environment.get_template(template)

    return HTMLBody(
        mail_template.render(orig_mail=original_html_mail, reply_content=reply_content)
    )
