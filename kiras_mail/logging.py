import logging

from kiras_mail.settings import settings

log = logging
log.basicConfig(level=settings.log_level)
