"""Reads Emails from exchange server."""

import time
from datetime import timedelta

import urllib3
from exchangelib import (
    DELEGATE,
    NTLM,
    Account,
    Configuration,
    Credentials,
    EWSDateTime,
    EWSTimeZone,
    Mailbox,
    Message,
)
from exchangelib.protocol import BaseProtocol, NoVerifyHTTPAdapter

from kiras_mail.kiras_api import KirasAPI
from kiras_mail.logging import log
from kiras_mail.mail import create_mail_body
from kiras_mail.settings import settings

# logging.basicConfig(level=logging.DEBUG, handlers=[PrettyXmlHandler()])
urllib3.disable_warnings()


BaseProtocol.HTTP_ADAPTER_CLS = NoVerifyHTTPAdapter

tz = EWSTimeZone("Europe/Berlin")

print("Email address:", settings.mail_address)

config = Configuration(
    server=settings.mail_server,
    credentials=Credentials(
        username=settings.user_name,
        password=settings.password,
    ),
    auth_type=NTLM,
)
log.info(config)
account = Account(
    primary_smtp_address=settings.mail_address, config=config, access_type=DELEGATE
)
log.info(account)

folder = account.inbox / settings.mailbox_folder
log.info(f"checking for mails in folder: {settings.mailbox_folder}")

feedback_folder = account.inbox / settings.feedback_folder
log.info(f"feedback folder: {settings.feedback_folder}")

blocked_folder = account.inbox / settings.blocked_folder

kiras_api = KirasAPI(settings.kiras_api_url)

if __name__ == "__main__":
    while True:
        now = EWSDateTime.now(tz)
        fifteen_minutes_ago = now - timedelta(minutes=15)

        new_mails = folder.filter(datetime_received__gt=fifteen_minutes_ago).order_by(
            "-datetime_received"
        )

        for mail in new_mails:
            if mail.subject is None:
                mail.subject = "Kein Betreff"

            log.debug(f"Processing mail: {mail.sender}: {mail.subject}")

            if settings.mail_suffix.lower() not in mail.sender.email_address.lower():
                mail.move(blocked_folder)
                log.info("Mail blocked: Unknown mail domain.")
                continue

            if mail.sender.email_address.lower() == settings.mail_address.lower():
                mail.move(blocked_folder)
                log.info("Mail blocked: Recipient and sender are same address.")
                continue

            if settings.subject_prefix in mail.subject:
                mail.move(feedback_folder)
                log.info("Mail contained subject prefix. Moved to feedback folder.")
                continue

            if mail.categories is not None and settings.environment in mail.categories:
                log.debug("Mail was already processed: Continuing to next.")
                continue

            log.info(f"Sending mail to API: From {mail.sender}: {mail.subject}")
            try:
                reply_content = kiras_api.collect_involvements(
                    text=mail.text_body, mail_subject=mail.subject
                )
                reply = Message(
                    account=account,
                    subject=settings.subject_prefix + " " + mail.subject,
                    body=create_mail_body(
                        original_html_mail=mail.body,
                        reply_content=reply_content,
                    ),
                    to_recipients=[Mailbox(email_address=mail.sender.email_address)],
                )
            except Exception as e:
                log.info(f"Problem handling mail {e}")
                reply = Message(
                    account=account,
                    subject=f"{settings.subject_prefix} {mail.subject}",
                    body=f"Bei Ihrer Anfrage ist ein technisches Problem aufgetreten. Bitte wenden Sie sich an {settings.admin_mail}."  # noqa: E501
                    + mail.text_body,
                    to_recipients=[Mailbox(email_address=mail.sender.email_address)],
                )

            reply.send_and_save()
            log.info("Reply sent.")
            if mail.categories is None:
                mail.categories = []
            mail.categories.append(settings.environment)
            mail.save()

        time.sleep(3)
