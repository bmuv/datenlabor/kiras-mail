"""loads settings and credentials."""

from pydantic import BaseSettings


class Settings(BaseSettings):
    """sets settings from env vars or file."""

    no_proxy: str = "http://intranet.office.dir"
    mail_server: str = "not-a-real-server"
    mail_address: str = "test@bmuv.bund.de"
    mail_suffix: str = "@bmuv.bund.de"
    user_name: str = "not-a-real-name"
    password: str = "not-a-real-password"
    environment: str = "testing"
    mailbox_folder: str = "development"
    blocked_folder: str = "blocked"
    subject_prefix: str = "[Beteiligungsvorschläge]"
    feedback_folder: str = "feedback"
    log_level: str = "INFO"
    kiras_api_url = "http://kiras-api:8000"
    kiras_api_login = "no-real-login"
    kiras_api_password = "not-a-real-password"
    admin_mail: str = "datenlabor@bmuv.bund.de"

    class Config:
        """defines file name."""

        env_file = ".env"
        env_file_encoding = "utf-8"


settings = Settings()
