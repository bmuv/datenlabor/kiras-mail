from typing import List, Optional

from pydantic import BaseModel


class InvolvementRequest(BaseModel):
    """request format of  KIRAS-api."""

    text: str
    top_k: int = 10
    agency: str


class Explanation(BaseModel):
    """Reasons for the match between organizational unit and text."""

    task: str
    sentence: str
    score: float


class InvolvementScore(BaseModel):
    """Involvement score for an organizational unit with regard to a given text."""

    unit_name: str
    score: float
    explanations: Optional[List[Explanation]] = None


class InvolvementResponse(BaseModel):
    """response from KIRAS-api."""

    text: str
    scores: List[InvolvementScore]


class Agency(BaseModel):
    """A governmental agency."""

    name: str
    abbreviation: str


class ReplySection(BaseModel):
    """Content of email block for one agency."""

    agency: Agency
    results: List[InvolvementScore]


class ReplyContent(BaseModel):
    """Content of reply email."""

    sections: List[ReplySection]
    email_text: str


class AgencyResponse(BaseModel):
    """Agency response from kiras-api."""

    agencies: List[Agency]
    default: str
